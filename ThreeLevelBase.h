#ifndef THREELEVELBASE_H
#define THREELEVELBASE_H

#include <cstddef>
#include <ostream>
#include "DiracBase.h"
#include "Parameters.h"

/*
 * Initializes the corresponding Bra or Ket for a given state of the system
 */
template<int n_max>
class ProductState {
private:
    Level i;
    int n;
public:
    inline ProductState(const int a_in) {
        i = Level(1 + a_in / (n_max + 1)); // 0, 0, ... 1, 1, ...., 1, 2, ... etc.
        n = a_in % (n_max + 1); // 0, 1, ... , maxPhotons, 0, 1, ..., maxPhotons etc.
    }

    inline const DoubleBra getBra() {
        return DoubleBra(Bra<electronic>(i), Bra<photonic>(n));
    }

    inline const DoubleKet getKet() {
        return DoubleKet(Ket<electronic>(i), Ket<photonic>(n));
    }

    inline const int getPhotons() const {
        return n;
    }

    inline const Level getLevel() const {
        return i;
    }

    inline friend std::ostream &operator<<(std::ostream &os, const ProductState &productState) {
        os << "state: ";
        switch (productState.i) {
            case level3:
                os << "state3";
                break;
            case level1:
                os << "state1";
                break;
            case level2:
                os << "state2";
                break;
        }
        os << " photons: " << productState.n;
        return os;
    }
};

//Abstract base class for all 3-level systems
class ThreeLevelBase {
protected:
    double delay = 0.0; //Needed when calculating spectra
    MatrixDiag H_0; //Non interacting Hamiltonian
    const Params params; //System Parameters

    const double E_1, E_2, E_3; //System energy levels

    //Calculate interacting Hamiltonian
    const Matrix getInteractionHamiltonian(const double t, const MatrixDiag &U, const MatrixDiag &U_adj) const;

    //Matrix Element of interacting Hamiltonian
    virtual const dcompl getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const =0;

    ThreeLevelBase( const Params &params, const double E_1, const double E_2, const double E_3);

public:
    //Called by odeint at every integration step
    virtual void operator()(const Matrix &Rho, Matrix &dRhodt, const double t);

    //Diagonal matrix for |1><1| etc.
    static const Matrix getOpMatDiag(const KetBra &kb);

    //Matrix for number operator
    template<photonOperatorType t>
    static const MatrixDiag getOpMatDiagType(const PhotonOperator<t> &p);

    //Matrix for creation & annihilation operators
    template<photonOperatorType t>
    static const Matrix getOpMat(const PhotonOperator<t> &p);

    //For |<P13b>|^2 etc.
    template<photonOperatorType t>
    static const Matrix getOpMat(const KetBra &kb, const PhotonOperator<t> &p);

    //Non Diagonal matrix for |1><2| etc.
    static const Matrix getOpMat(const KetBra &kb);

    //Diagonal Matrix of Number Operator
    static const MatrixDiag getOpMatDiagType(const NumberOperator &p);

    //Dissipative terms, Rho has to be in Schrödinger picture
    const Matrix LCavity(const Matrix &Rho) const;

    const Matrix LRad(const Matrix &Rho) const;

    const Matrix LPure(const Matrix &Rho) const;

    //Needed for calculation of g1, for calculating RhoTilde
    inline void setDelay(double delay) {
        ThreeLevelBase::delay = delay;
    }

    //Density Matrix at t = 0;
    virtual const Matrix getRho0() const;

    //Time evolution operator
    //Mind the sign convention here
    inline const MatrixDiag getU(const double t) const {
        return matrixExp(H_0, im / hBar * t);
    }

    inline const double getE_1() const {
        return E_1;
    }

    inline const double getE_2() const {
        return E_2;
    }

    inline const double getE_3() const {
        return E_3;
    }

    //Laser frequency (including detuning)
    inline const double getHwl() const {
        return abs(E_1 - E_3 - params.getHw_c()) + params.getLaserDetuning();
    }

    virtual ~ThreeLevelBase() {}
};

template<photonOperatorType t>
const Matrix ThreeLevelBase::getOpMat(const PhotonOperator<t> &p) {
    Matrix Mat(0.0);
    for (int i = 0; i < maxStates; ++i) {
        for (int j = 0; j < maxStates; ++j) {
            ProductState<maxPhotons> p1(i), p2(j);
            Mat(i, j) = p1.getBra() * (p * p2.getKet());
        }
    }
    return Mat;
}

template<photonOperatorType t>
const Matrix ThreeLevelBase::getOpMat(const KetBra &kb, const PhotonOperator<t> &p) {
    Matrix Mat(0.0);
    for (int i = 0; i < maxStates; ++i) {
        for (int j = 0; j < maxStates; ++j) {
            ProductState<maxPhotons> p1(i), p2(j);
            Mat(i, j) = p1.getBra() * kb * (p * p2.getKet());
        }
    }
    return Mat;
}

class LambdaSystem : public ThreeLevelBase {
private:
    static constexpr double E_1 = 1.45e-3;
    static constexpr double E_2 = 1.296;
    static constexpr double E_3 = 0.0;

    virtual const dcompl getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const;

public:
    LambdaSystem( const Params &params);

    virtual ~LambdaSystem() {}
};

class XiSystem : public ThreeLevelBase {
private:
    static constexpr double E_1 = 2 * 1.296 - 1.45e-3;
    static constexpr double E_2 = 1.296;
    static constexpr double E_3 = 0.0;

    virtual const dcompl getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const;

public:
    XiSystem( const Params &params);

    virtual ~XiSystem() {}
};

class VSystem : public ThreeLevelBase {
private:
    static constexpr double E_1 = 1.45e-3;
    static constexpr double E_2 = -1.296 + 1.45e-3;  /
    static constexpr double E_3 = 0.0;

    virtual const dcompl getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const;

public:
    VSystem( const Params &params);

    virtual ~VSystem() {}
};

class Xi2System : public ThreeLevelBase {
private:
    static constexpr double E_1 = 2 * 1.296 - 1.45e-3;
    static constexpr double E_2 = 1.296 - 1.45e-3;
    static constexpr double E_3 = 0.0;

    virtual const dcompl getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const;

public:
    Xi2System( const Params &params);

    virtual ~Xi2System() {}
};

class TwoLevelSystem : public ThreeLevelBase {
private:
    static constexpr double E_1 = 1.296;
    static constexpr double E_2 = 0.0;
    static constexpr double E_3 = 0.0;

    virtual const dcompl getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const;

public:
    TwoLevelSystem( const Params &params);

    virtual ~TwoLevelSystem() {}
};

#endif //THREELEVELBASE_H
