#ifndef SAVEVALUES_H
#define SAVEVALUES_H

#include <map>
#include <string>
#include <vector>
#include <fstream>
#include "Parameters.h"
#include "ThreeLevelBase.h"

namespace OpLabels {
    enum expectationValue {
        bb, s1, s2, s3, P32b, P31b, P21b, sigma32, sigma21, sigma32bb, sigma21bb, b, bk
    };
}


class SaveValues {
private:
    //pointers!
    std::shared_ptr<std::map<OpLabels::expectationValue, Matrix>> mapOperators;
    std::shared_ptr<std::map<OpLabels::expectationValue, std::vector<dcompl>>> mapValues;
    std::shared_ptr<std::vector<double>> times;
    Params params;
    ThreeLevelBase *vn;

    std::vector<OpLabels::expectationValue> names = {OpLabels::bb, OpLabels::s1, OpLabels::s2, OpLabels::s3, OpLabels::P32b, OpLabels::P31b,
                                                OpLabels::P21b, OpLabels::sigma32, OpLabels::sigma21, OpLabels::sigma32bb,
                                                OpLabels::sigma21bb, OpLabels::b, OpLabels::bk};

    bool output; //Display current t in console?

    inline double getStep() const { return times.get()->at(1); }

public:
    inline std::vector<dcompl> get_bb() const { return mapValues.get()->at(OpLabels::bb); }

    inline std::vector<dcompl> get_b() const { return mapValues.get()->at(OpLabels::b); }

    inline std::vector<dcompl> get_bk() const { return mapValues.get()->at(OpLabels::bk); }

    inline std::vector<dcompl> get_s1() const { return mapValues.get()->at(OpLabels::s1); }

    inline std::vector<dcompl> get_s2() const { return mapValues.get()->at(OpLabels::s2); }

    inline std::vector<dcompl> get_s3() const { return mapValues.get()->at(OpLabels::s3); }

    inline std::vector<dcompl> get_P32b() const { return mapValues.get()->at(OpLabels::P32b); }

    inline std::vector<dcompl> get_P31b() const { return mapValues.get()->at(OpLabels::P31b); }

    inline std::vector<dcompl> get_P21b() const { return mapValues.get()->at(OpLabels::P21b); }

    inline std::vector<dcompl> get_sigma32() const { return mapValues.get()->at(OpLabels::sigma32); }

    inline std::vector<dcompl> get_sigma21() const { return mapValues.get()->at(OpLabels::sigma21); }

    inline std::vector<dcompl> get_sigma21bb() const { return mapValues.get()->at(OpLabels::sigma21bb); }

    inline std::vector<dcompl> get_sigma32bb() const { return mapValues.get()->at(OpLabels::sigma32bb); }

    SaveValues(bool output, const Params &params, ThreeLevelBase *vn);

    //Called by Odeint at every step
    void operator()(const Matrix &Rho, const double &t);

    //Writes the expectation values to (name).txt
    void writeFile(const std::string &name) const;

    inline std::vector<double> getTimes() {
        return *times;
    }

    inline std::map<OpLabels::expectationValue, std::vector<dcompl>> getValues() {
        return *(mapValues.get());
    };

    inline std::vector<dcompl> *getExpectationValuePointer(OpLabels::expectationValue lab) {
        return &mapValues.get()->at(lab);
    }

    std::vector<double> calcRamanPopulation() const;

    std::vector<double> calcElectronicPopulation() const;

    std::vector<double> calcCavityPhotonPopulation() const;

    std::vector<double> calcTripletPhotonPopulation() const;

    const double calcEmissionProbability(const std::vector<dcompl> &photonVec, const double step) const;

    const double calcEmissionProbability(const std::vector<double> &photonVec, const double step) const;

    std::vector<double> calcAnalyticalPhotonPopulation() const;

    std::vector<double> analyticalPhotonIntegral(const std::vector<dcompl> &PiVec) const;

    const double tlsAnalyticalInversion(const double t) const;

    const double tlsAnalyticalState2(const double t) const;

    const double tlsAnalyticalState1(const double t) const;

    const double tlsAnalyticalPhotons(const double t) const;
};

#endif //SAVEVALUES_H
