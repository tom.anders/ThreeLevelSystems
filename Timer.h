#ifndef TIMER_H
#define TIMER_H

#include <ctime>
#include <iostream>
#include <omp.h>

class Timer {
private:
    double start;
public:
    Timer();
    void stop(const std::string &name = "time");
};

#endif //TIMER_H
