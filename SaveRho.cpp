#include "SaveRho.h"

SaveRho::SaveRho(ThreeLevelBase *vnI, bool output) : vn(vnI), output(output), rho(new std::vector<Matrix>) {}

void SaveRho::operator()(const Matrix &Rho, double t) {
    rho->push_back(Rho);
    if (output) std::cout << t << std::endl;
}


