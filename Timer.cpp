#include "Timer.h"

Timer::Timer() : start(omp_get_wtime()) {}

void Timer::stop(const std::string &name) {
    std::cout << name << " " << omp_get_wtime() - start  << "s"<< std::endl;
    start = omp_get_wtime();
}
