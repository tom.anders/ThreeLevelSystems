#ifndef NUMERICALINTEGRATION_H
#define NUMERICALINTEGRATION_H

#include <vector>
#include <complex>
#include <functional>

typedef std::complex<double> dcompl;


//Three methods which solve the equation g(t) = \int_0^t f(t') dt' via simpsons's rule and return g(t) as a std::vector<dcompl>:
std::vector<dcompl> simpsonToT(const std::vector<dcompl> &vec, double h);
std::vector<dcompl> simpsonToT(std::function<dcompl(double)> f, double h, unsigned long max);
std::vector<dcompl> simpsonToT(std::function<dcompl(unsigned long)> f, unsigned long max, double h);

//Integrate the vector with step size h via simpson's rule
template<typename T>
inline T simpson(const std::vector<T> &vec, double h) {
    unsigned long max = vec.size();
    T sum = 0.0;
    if(max % 2 == 0) {
        for(unsigned long i = 0; i < max - 2; i+=2) {
            sum += vec.at(i) + 4.0 * vec.at(i + 1) + vec.at(i + 2);
        }
        sum *= h / 3.0;
        sum += 0.5*h * (vec.at(max-2) + vec.at(max-1));
    } else {
        for(unsigned long i = 0; i < max -1; i+=2) {
            sum += vec.at(i) + 4.0 * vec.at(i + 1) + vec.at(i + 2);
        }
        sum *= h / 3.0;
    }
    return sum;
}

//Multiplies the std::vector with a function f(t) where t = step*index
const std::vector<dcompl> multiplVector(const std::vector<dcompl> &vec, std::function<dcompl(double)> f, double step);

//Same thing but only takes the index, so t has to be calculated inside f
const std::vector<dcompl> multiplVector(const std::vector<dcompl> &vec, std::function<dcompl(int)> f);
//Multiplies by a function and a second vector (element-wise)
const std::vector<dcompl> multiplVector(const std::vector<dcompl> &vec1, const std::vector<dcompl> &vec2, std::function<dcompl(int)> f);
//Subtracts vec2 from vec1 (element-wise)
const std::vector<dcompl> subtVector(const std::vector<dcompl> &vec1, const std::vector<dcompl> &vec2);

#endif //NUMERICALINTEGRATION_H
