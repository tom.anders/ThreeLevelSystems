#ifndef MATRIX_COMPLEX_H
#define MATRIX_COMPLEX_H

#include <complex>
#include <vector>
#include <boost/operators.hpp>
#include <boost/numeric/odeint.hpp>

typedef std::complex<double> dcompl;

//[Matrix
template<unsigned int n, bool diagonal>
class MatrixComplex :
        boost::additive1<MatrixComplex<n, diagonal>,
                boost::additive2<MatrixComplex<n, diagonal>, dcompl,
                        boost::multiplicative2<MatrixComplex<n, diagonal>, dcompl> > > {
private:
    std::array<dcompl, n * n> arr;
public:

    MatrixComplex() {}

    MatrixComplex(const dcompl& val) {
        for (int i = 0; i < n * n; i++)
            arr[i] = val;
    }

    MatrixComplex &operator+=(const MatrixComplex &m) {
        for (int i = 0; i < n * n; i++)
            arr[i] += m.arr[i];
        return *this;
    }

    MatrixComplex &operator-=(const MatrixComplex &m) {
        for (int i = 0; i < n * n; i++)
            arr[i] -= m.arr[i];
        return *this;
    }

    MatrixComplex &operator*=(const dcompl& a) {
        for (int i = 0; i < n * n; i++)
            arr[i] *= a;
        return *this;
    }

    dcompl &operator[](const int i) {
        return arr[i];
    }

    const dcompl operator[](const int i) const {
        return arr[i];
    }

    dcompl &operator()(const int i, const int j) { return arr[i * n + j]; }

    const dcompl operator()(const int i, const int j) const { return arr[i * n + j]; }

    const double getMax() const {
        std::vector<double> m(n * n);
        for (int i = 0; i < n * n; i++)
            m[i] = std::abs(arr[i]);
        return *std::max_element(m.begin(), m.end());
    }

    const unsigned int getSize() const {
        return n * n;
    }

    const std::complex<double> trace() const {
        std::complex<double> tr = 0.0;
        for (int i = 0; i < n; i++)
            tr += arr[i * n + i];
        return tr;
    }

};
//]

//[Matrix_abs_div
// only required for steppers with error control
template<unsigned int n, bool diagonal>
MatrixComplex<n, diagonal> operator/(const MatrixComplex<n, diagonal> &m1, const MatrixComplex<n, diagonal> &m2) {
    MatrixComplex<n, diagonal> out;
    for (int i = 0; i < n * n; i++)
        out[i] = m1[i] / m2[i];
    return out;
}

template<unsigned int n, bool diagonal>
MatrixComplex<n, diagonal> abs(const MatrixComplex<n, diagonal> &p) {
    MatrixComplex<n, diagonal> out;
    for (int i = 0; i < n * n; i++)
        out[i] = std::abs(p[i]);
    return out;
}
//]

//[Matrix_norm
// also only for steppers with error control
namespace boost {
    namespace numeric {
        namespace odeint {
            template<unsigned int n, bool diagonal>
            struct vector_space_norm_inf<MatrixComplex<n, diagonal> > {
                typedef double result_type;

                double operator()(const MatrixComplex<n, diagonal> &p) const {
                    return p.getMax();
                }
            };
        }
    }
}
//]

template<unsigned int n, bool diagonal>
std::ostream &operator<<(std::ostream &out, const MatrixComplex<n, diagonal> &m) {
    out.precision(8);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)
            out << std::setw(16) << m(i, j) << " ";
        out << std::endl;
    }
    out << std::endl;
    return out;
}

template<unsigned int n>
MatrixComplex<n, false> operator*(const MatrixComplex<n, false> &m1, const MatrixComplex<n, true> &m2) {
    MatrixComplex<n, false> p;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            p(i, j) = m1(i, j) * m2(j, j);
    return p;
}

template<unsigned int n>
MatrixComplex<n, false> operator*(const MatrixComplex<n, true> &m1, const MatrixComplex<n, false> &m2) {
    MatrixComplex<n, false> p;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            p(i, j) = m1(i,i) * m2(i, j);
    return p;
}

template<unsigned int n>
MatrixComplex<n, false> operator*(const MatrixComplex<n, false> &m1, const MatrixComplex<n, false> &m2) {
    MatrixComplex<n, false> p;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            std::complex<double> sum = 0.0;
            for (int k = 0; k < n; ++k) {
                sum += m1(i, k) * m2(k, j);
            }
            p(i, j) = sum;
        }
    }
    return p;
}

//Only for diagonal matrices
template<unsigned int n>
MatrixComplex<n, true> matrixExp(const MatrixComplex<n, true> &m, const dcompl &factor) {
    MatrixComplex<n, true> e;
    for (unsigned int i = 0; i < n; ++i)
        e(i, i) = exp(factor * m(i, i));
    return e;
}

template<unsigned int n>
MatrixComplex<n, true> complexConjugateDiagonal(const MatrixComplex<n, true> &m) {
    MatrixComplex<n, true> cc;
    for (int j = 0; j < n; ++j) {
        cc(j, j) = std::conj(m(j, j));
    }
    return cc;
}

#endif // MATRIX_COMPLEX_H
