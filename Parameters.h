#ifndef PARAMS_H
#define PARAMS_H

#include <complex>
#include <ostream>
#include "MatrixComplex.h"
#include "tclap/CmdLine.h"

typedef std::complex<double> dcompl;

enum SystemType {
    Lambda, Xi, V, Xi2, TwoLevel
};

template<typename T>
std::string to_string_with_precision(const T a_value, const int n = 6) {
    std::ostringstream out;
    out << std::setprecision(n) << a_value;
    return out.str();
}

const double hBar = 6.582119514e-4; // [eV*ps]
const dcompl im(0.0, 1.0); //imaginary unit
const int maxPhotons = 1; //2;
const int maxStates = 3 * (maxPhotons + 1);

typedef MatrixComplex<maxStates, false> Matrix;
typedef MatrixComplex<maxStates, true> MatrixDiag;

class Params {
private:
    const double g = 57.0e-6; // [eV]
    double kappa = 186.0e-6; // [eV]
    double gammaPure = 30.0e-6; // [eV]
    double gammaRad = 30.0e-6; // [eV]
    double cavityDetuning = -2.0e-3; // [eV]
    double hw_c = 1.296; // [eV
    double laserDetuning;

    bool useKappa = false;
    bool useGammaPure = false;
    bool useGammaRad = false;
    bool useOptimalDetuning = false;
    bool noRWA = false;
    bool timeGt = false;

    bool useRectanglePulse = false;
    bool useGaussPulse = false;
    double deltaT = 4.0; //[ps]
    double omega0 = 0.2e-3; //[eV]
    double t0 = 15.0; //[ps]
    double rectLength = 18.0;

    double area = 0.0;

    double tMax = 100.0; //integration time
    double integrationStep = 0.0;
    int initialPhotons = 0;

    std::string systemName;
    std::string systemGnuplotLabel;
    SystemType systemType;
    std::string normalizePart;
    std::string filename;


    double gauss(double t) const;

    double rect(double t) const;

public:
    Params(TCLAP::CmdLine &cmd, int argc, char **argv);

    //(time dependent) amplitude of our laser, cw or gauss
    dcompl laser(double t) const;

    friend std::ostream &operator<<(std::ostream &os, const Params &params);

    /*
     * Getters and Setters
     */

    inline const double getG() const { return g; }

    inline double getIntegrationStep() const { return integrationStep; }

    inline const double getKappa() const { return kappa; }

    inline const double getGammaPure() const { return gammaPure; }

    inline const double getGammaRad() const { return gammaRad; }

    inline const double getHw_c() const { return hw_c + cavityDetuning; }

    inline const int getInitialPhotons() const { return initialPhotons; }

    inline const bool UseKappa() const { return useKappa; }

    inline const bool UseGammaPure() const { return useGammaPure; }

    inline const bool UseGammaRad() const { return useGammaRad; }

    inline const bool UsePulse() const { return useRectanglePulse || useGaussPulse; }

    inline const double getOmega0() const { return omega0; }

    inline void setOmega0(const double omega0) { Params::omega0 = omega0; }

    void setSystemName(std::string &system);

    const std::string getPulseName() const;

    inline const std::string getSystemName() const { return systemName; }

    inline const SystemType getSystemType() const { return systemType; }

    inline const double getOptimalDetuning() {
        return abs(cavityDetuning) * pow(omega0, 2.) / (pow(cavityDetuning, 2.) + pow(kappa / 2., 2.)) * 1.0e3;
    }

    inline const double getTMax() const { return tMax; }

    inline void setTMax(const double tMax) { Params::tMax = tMax; }

    inline const bool rectanglePulse() const { return useRectanglePulse; }

    inline const double getCavityDetuning() const { return cavityDetuning; }

    inline void setLaserDetuning(const double laserDetuning) { Params::laserDetuning = laserDetuning; }

    inline void setSystemType(const SystemType type) { systemType = type; }

    void setPulseArea(const double newArea);

    double getPulseArea() const;

    inline bool useNoRWA() const {return noRWA; }

    inline void setCavityDetuning(double cavityDetuning) { Params::cavityDetuning = cavityDetuning; }

    inline const std::string &getNormalizePart() const { return normalizePart; }

    inline bool useTimeGt() const {
        return timeGt;
    }

    double getLaserDetuning() const {
        return laserDetuning;
    }

    inline const std::string &getFilename() const { return filename; }
};

#endif //PARAMS_H
