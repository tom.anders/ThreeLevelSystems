#ifndef SAVERHO_H
#define SAVERHO_H

#include <vector>
#include <fstream>
#include <memory>
#include "ThreeLevelBase.h"
#include "MatrixComplex.h"

class SaveRho {
private:
    ThreeLevelBase *vn;
    std::shared_ptr<std::vector<Matrix>> rho;
    bool output;
public:
    SaveRho(ThreeLevelBase *vnI, bool output = true);

    //Called by odeint at every step
    void operator()(const Matrix &Rho, double t);

    inline const std::vector<Matrix> *getRho() const {
        return rho.get();
    }
};

#endif //SAVERHO_H
