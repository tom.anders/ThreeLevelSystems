#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

#include <boost/numeric/odeint.hpp>
#include "Parameters.h"
#include "ThreeLevelBase.h"
#include "SaveValues.h"
#include "SaveRho.h"

typedef boost::numeric::odeint::runge_kutta_dopri5<Matrix, double, Matrix, double, boost::numeric::odeint::vector_space_algebra> stepper;

void integrateConst(SystemType type, ThreeLevelBase *vn, Matrix &Rho0, double t0, double tMax, double step,
                    SaveRho &sr);

void integrateConst(SystemType type, ThreeLevelBase *vn, Matrix &Rho0, double t0, double tMax, double step,
                    SaveValues &sr);

ThreeLevelBase* initThreeLevelBase(SystemType type,  const Params &params);


#endif //RUNGEKUTTA_H
