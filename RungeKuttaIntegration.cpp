#include "RungeKuttaIntegration.h"

void integrateConst(SystemType type, ThreeLevelBase *vn, Matrix &Rho0, double t0, double tMax, double step, SaveValues &sr) {
    using boost::numeric::odeint::integrate_adaptive;
    switch (type) {
        case Lambda:
            integrate_adaptive(stepper(), *(dynamic_cast<LambdaSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case Xi:
            integrate_adaptive(stepper(), *(dynamic_cast<XiSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case V:
            integrate_adaptive(stepper(), *(dynamic_cast<VSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case Xi2:
            integrate_adaptive(stepper(), *(dynamic_cast<Xi2System *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case TwoLevel:
            integrate_adaptive(stepper(), *(dynamic_cast<TwoLevelSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
    }
}

void integrateConst(SystemType type, ThreeLevelBase *vn, Matrix &Rho0, double t0, double tMax, double step, SaveRho &sr) {
    using boost::numeric::odeint::integrate_adaptive;
    switch (type) {
        case Lambda:
            integrate_adaptive(stepper(), *(dynamic_cast<LambdaSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case Xi:
            integrate_adaptive(stepper(), *(dynamic_cast<XiSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case V:
            integrate_adaptive(stepper(), *(dynamic_cast<VSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case Xi2:
            integrate_adaptive(stepper(), *(dynamic_cast<Xi2System *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
        case TwoLevel:
            integrate_adaptive(stepper(), *(dynamic_cast<TwoLevelSystem *>(vn)),
                               Rho0, t0, tMax, step, sr);
            break;
    }
}

ThreeLevelBase *initThreeLevelBase(SystemType type,  const Params &params) {
    switch (type) {
        case Lambda:
            return new LambdaSystem( params);
        case Xi:
            return new XiSystem( params);
        case Xi2:
            return new Xi2System( params);
        case V:
            return new VSystem( params);
        case TwoLevel:
            return new TwoLevelSystem( params);
    }
}

