#include <stdexcept>
#include "NumericalIntegration.h"

const std::vector <dcompl> multiplVector(const std::vector <dcompl> &vec, std::function<dcompl(double)> f, double step) {
    std::vector <dcompl> res = vec;
    for (int i = 0; i < vec.size(); i++)
        res[i] *= f((double) i * step);
    return res;
}

const std::vector <dcompl> multiplVector(const std::vector <dcompl> &vec, std::function<dcompl(int)> f) {
    std::vector <dcompl> res = vec;
    for (int i = 0; i < vec.size(); i++)
        res[i] *= f(i);
    return res;
}

const std::vector <dcompl>
multiplVector(const std::vector <dcompl> &vec1, const std::vector <dcompl> &vec2, std::function<dcompl(int)> f) {
    if (vec1.size() != vec2.size())
        throw std::invalid_argument("sizes do not match");
    std::vector <dcompl> res = vec1;
    for (int i = 0; i < vec1.size(); i++)
        res[i] *= vec2[i] * f(i);
    return res;
}

const std::vector <dcompl> subtVector(const std::vector <dcompl> &vec1, const std::vector <dcompl> &vec2) {
    if (vec1.size() != vec2.size())
        throw std::invalid_argument("sizes do not match");
    std::vector <dcompl> res = vec1;
    for (int i = 0; i < vec1.size(); i++)
        res[i] -= vec2[i];
    return res;
}

std::vector <dcompl> simpsonToT(const std::vector <dcompl> &vec, double h) {
    unsigned long max = vec.size();
    std::vector <dcompl> res(max);
    res.at(0) = 0.0;
    dcompl sum = 0.0;
    if (max % 2 == 0) {
        for (unsigned long i = 0; i < max - 2; i += 2) {
            sum += vec.at(i) + 4.0 * vec.at(i + 1) + vec.at(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
        res.at(1) = 0.5 * h * (vec.at(0) + vec.at(1)); //first step trapezoidal rule
        sum = 0.0;
        for (unsigned long i = 1; i < max - 1; i += 2) {
            sum += vec.at(i) + 4.0 * vec.at(i + 1) + vec.at(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
    } else {
        for (unsigned long i = 0; i < max - 1; i += 2) {
            sum += vec.at(i) + 4.0 * vec.at(i + 1) + vec.at(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
        res.at(1) = 0.5 * h * (vec.at(0) + vec.at(1)); //first step trapezoidal rule
        sum = 0.0;
        for (unsigned long i = 1; i < max - 2; i += 2) {
            sum += vec.at(i) + 4.0 * vec.at(i + 1) + vec.at(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
    }
    return res;
}

std::vector <dcompl> simpsonToT(std::function<dcompl(double)> f, double h, unsigned int max) {
    std::vector <dcompl> res(max);
    res.at(0) = 0.0;
    if (max == 1)
        return res;
    dcompl sum = 0.0;
    if (max % 2 == 0) {
        for (unsigned long i = 0; i < max - 2; i += 2) {
            double t = (double) i * h;
            sum += f(t) + 4.0 * f(t + h) + f(t + 2.0 * h);
            res.at(i + 2) = h / 3.0 * sum;
        }
        res.at(1) = 0.5 * h * (f(0.0) + f(h)); //first step trapezoidal rule
        sum = 0.0;
        for (unsigned long i = 1; i < max - 1; i += 2) {
            double t = (double) i * h;
            sum += f(t) + 4.0 * f(t + h) + f(t + 2.0 * h);
            res.at(i + 2) = h / 3.0 * sum;
        }
    } else {
        for (unsigned long i = 0; i < max - 1; i += 2) {
            double t = (double) i * h;
            sum += f(t) + 4.0 * f(t + h) + f(t + 2.0 * h);
            res.at(i + 2) = h / 3.0 * sum;
        }
        res.at(1) = 0.5 * h * (f(0.0) + f(h)); //first step trapezoidal rule
        sum = 0.0;
        for (unsigned long i = 1; i < max - 2; i += 2) {
            double t = (double) i * h;
            sum += f(t) + 4.0 * f(t + h) + f(t + 2.0 * h);
            res.at(i + 2) = h / 3.0 * sum;
        }
    }
    return res;
}

std::vector <dcompl> simpsonToT(std::function<dcompl(unsigned long)> f, unsigned long max, double h) {
    std::vector <dcompl> res(max);
    res.at(0) = 0.0;
    if (max == 1)
        return res;
    dcompl sum = 0.0;
    if (max % 2 == 0) {
        for (unsigned long i = 0; i < max - 2; i += 2) {
            sum += f(i) + 4.0 * f(i + 1) + f(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
        res.at(1) = 0.5 * h * (f(0) + f(1)); //first step trapezoidal rule
        sum = 0.0;
        for (unsigned long i = 1; i < max - 1; i += 2) {
            sum += f(i) + 4.0 * f(i + 1) + f(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
    } else {
        for (unsigned long i = 0; i < max - 1; i += 2) {
            sum += f(i) + 4.0 * f(i + 1) + f(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
        res.at(1) = 0.5 * h * (f(0) + f(1)); //first step trapezoidal rule
        sum = 0.0;
        for (unsigned long i = 1; i < max - 2; i += 2) {
            sum += f(i) + 4.0 * f(i + 1) + f(i + 2);
            res.at(i + 2) = h / 3.0 * sum;
        }
    }
    return res;
}

