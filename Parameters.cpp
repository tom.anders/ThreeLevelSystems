#include "Parameters.h"

double Params::gauss(double t) const {
    return exp(-0.5 * pow(((t - t0) / deltaT), 2.0));
}

double Params::rect(double t) const {
    double rectStart = 1.0;
    if (t < rectStart) {
        return exp(-0.5 * pow(((t - rectStart) / 0.3), 2.0));
    } else if (t > 1.0 + rectLength) {
        return exp(-0.5 * pow(((t - rectStart - rectLength) / 0.3), 2.0));;
    } else {
        return 1.0;
    }
}

Params::Params(TCLAP::CmdLine &cmd, int argc, char **argv) {
    using namespace TCLAP;
    SwitchArg kappaSwitch("k", "useKappa", "", cmd, false);
    SwitchArg gammaPureSwitch("g", "useGammaPure", "", cmd, false);
    SwitchArg gammaRadSwitch("", "useGammaRad", "", cmd, false);
    SwitchArg gaussPulseSwitch("", "gauss", "", cmd, false);
    SwitchArg rectanglePulseSwitch("r", "rect", "", cmd, false);
    SwitchArg rwaSwitch("", "noRWA", "", cmd, false);
    SwitchArg gtSwitch("", "gt", "output time in units of g*t", cmd, false);

    ValueArg<double> kappaValue("", "setKappa", "", false, kappa * 1e6, "kappa in micro eV", cmd);
    ValueArg<double> gammaPureValue("", "setGammaPure", "", false, gammaPure * 1e6, "gammaPure in micro eV", cmd);
    ValueArg<double> gammaRadValue("", "setGammaRad", "", false, gammaRad * 1e6, "gammaRad in micro eV", cmd);
    ValueArg<double> omegaValue("o", "setOmega", "", false, omega0 * 1e3, "omega0 in meV", cmd);
    ValueArg<double> areaValue("a", "setArea", "", false, 0.0, "pulse area in pi", cmd);
    ValueArg<double> t0Value("t", "setT0", "", false, t0, "t0 in ps (for pulse)", cmd);
    ValueArg<double> deltaTValue("", "setDeltaT", "", false, deltaT, "pulse width in ps", cmd);
    ValueArg<double> cavityDetuningValue("", "setCavityDetuning", "", false, cavityDetuning * 1e3, "cavity detuning in meV", cmd);
    ValueArg<double> tMaxValue("", "setTMax", "", false, tMax, "integration time in ps", cmd);
    ValueArg<double> rectLengthValue("", "setRectL", "", false, rectLength, "rect pulse max in ps", cmd);
    ValueArg<double> integrationStepValue("", "step", "", false, 0.05,
                                          "integration step for odeint, some methods may still use hardcoded values!", cmd);
    ValueArg<double> laserDetuningValue("d", "setLaserDetuning", "", false, 0.0, "laser detuning in meV", cmd);
    ValueArg<int> initialPhotonsValue("i", "initialPhotons", "", false, 0,
                                          "photons in cavity at t=0", cmd);
    ValueArg<std::string> normalizeValue("n", "normalize", "cavity, raman, tls or rest", false, "", "normalize to spectrum part", cmd);
    ValueArg<std::string> fileNameValue("f", "fileName", "", false, "values", "filename", cmd);
    SwitchArg optimalDetuningSwitch("", "optimalDetuning", "use analytical optimal detuning", cmd, false);

    cmd.parse(argc, (const char *const *) argv);

    useKappa = kappaSwitch.getValue();
    useGammaPure = gammaPureSwitch.getValue();
    useGammaRad = gammaRadSwitch.getValue();
    useGaussPulse = gaussPulseSwitch.getValue();
    useRectanglePulse = rectanglePulseSwitch.getValue();
    noRWA = rwaSwitch.getValue();
    timeGt = gtSwitch.getValue();

    tMax = tMaxValue.getValue();
    rectLength = rectLengthValue.getValue();

    initialPhotons = initialPhotonsValue.getValue();

    integrationStep = integrationStepValue.getValue();

    normalizePart = normalizeValue.getValue();
    filename = fileNameValue.getValue();

    kappa = kappaValue.getValue() * 1e-6;
    gammaPure = gammaPureValue.getValue() * 1e-6;
    gammaRad = gammaRadValue.getValue() * 1e-6;
    omega0 = omegaValue.getValue() * 1e-3;
    setPulseArea(areaValue.getValue());
    t0 = t0Value.getValue();
    deltaT = deltaTValue.getValue();
    cavityDetuning = cavityDetuningValue.getValue() * 1e-3;

    double detuning = optimalDetuningSwitch.getValue() ? getOptimalDetuning() : laserDetuningValue.getValue();
    useOptimalDetuning = optimalDetuningSwitch.getValue();
    laserDetuning = detuning * 1.0e-3;
}

dcompl Params::laser(double t) const {
    if (useRectanglePulse)
        return omega0 * rect(t);
    else if (useGaussPulse)
        return omega0 * gauss(t);

    return omega0;
}

void Params::setSystemName(std::string &system) {
    systemName = system;
    if (system == "lambda")
        system = "Λ";
    else if (system == "xi")
        system = "Ξ";
    else if (system == "xi2")
        system = "Ξ_2";
    else if (system == "v")
        system = "V";
    Params::systemGnuplotLabel = system;
}

const std::string Params::getPulseName() const {
    if (useRectanglePulse)
        return "rect";
    if (useGaussPulse)
        return "gauss";
    return "cw";
}

std::ostream &operator<<(std::ostream &os, const Params &params) {
    os << "System: " << params.systemGnuplotLabel << ", ";
    std::string pulse;
    if (params.useRectanglePulse)
        pulse = "rect";
    else if (params.useGaussPulse)
        pulse = "gauss";
    else
        pulse = "cw";
    if (params.useKappa)
        os << " κ = " << params.kappa * 1.0e6 << "μeV ";
    if (params.useGammaRad)
        os << ", γ_{rad} = " << params.gammaRad * 1.0e6 << "μeV ";
    if (params.useGammaPure)
        os << ", γ_{pure} = " << params.gammaPure * 1.0e6 << "μeV ";
    os << ", g = " << params.g * 1.0e6 << "μev " << ", Δ_c = " << params.cavityDetuning * 1.0e3 << "meV "
       << ", ħω_c = " << params.hw_c
       << "meV " << ", Pulse: " << pulse << ", Ω_0 = " << params.omega0 * 1.0e3 << "meV";
    os << ", Δ_L = " << params.laserDetuning << "meV ";
    if (params.useOptimalDetuning)
        os << "(optimal) ";
    if (params.useGaussPulse && !params.useRectanglePulse)
        os << ", deltaT = " << params.deltaT << "ps" << ", t0 = " << params.t0 << "ps";
    else if (params.useRectanglePulse)
        os << ", rectLength = " << params.rectLength << "ps";
    os << ", tMax = " << params.tMax << "ps" << ", step = " << params.integrationStep << "ps";
    os << ", pulse aerea(pi) = " << params.getPulseArea() << std::endl;

    return os;
}

double Params::getPulseArea() const {
    if(area != 0.0)
        return area;
    if (useRectanglePulse)
        return 2.0/hBar * 0.3 * sqrt(2/M_PI) * omega0 +  2.0/hBar * rectLength * omega0 / M_PI;
    else if (useGaussPulse)
        return 2.0/hBar * deltaT * sqrt(2/M_PI) * omega0;
    else throw std::invalid_argument("getPulseArea");
}

void Params::setPulseArea(const double newArea) {
    area = newArea;
    if(area != 0.0) {
        if(useRectanglePulse)
            omega0 = area * M_PI * hBar / (2.0 * (0.3*sqrt(2.0*M_PI) + rectLength));
        else
            omega0 = area * M_PI * hBar / (2.0 * deltaT * sqrt(2.0*M_PI));
    }
}


