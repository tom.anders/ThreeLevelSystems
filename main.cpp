#include "main.h"
#include "SaveValues.h"
#include "SaveRho.h"
#include "Timer.h"
#include "SpectrumCalculator.h"

int main(int argc, char **argv) {
    //Parse command line arguments
    SystemType type;

    using namespace TCLAP;
    CmdLine cmd("", ' ', "1.0");

    SwitchArg photonMapSwitch("", "photon", "Time evolution of photon population for different laser detunings", cmd,
                              false);
    SwitchArg g1Switch("", "g1", "Calculate autocorrelation & spectrum decomposition", cmd, false);
    SwitchArg mapPlotSwitch("", "map", "Calculate Emission probability for varying pulse area and laser detuning", cmd,
                            false);
    SwitchArg singleSwitch("", "single",
                           "Calculate time evolution of various expectation values (photon populatin etc.)", cmd,
                           false);
    SwitchArg spectrumTimeSwitch("", "spectrumTime", "Spectrum at different times", cmd, false);
    SwitchArg lvcSwitch("", "lvc", "Calculate emission probability for varying laser detuning and cavity detuning", cmd,
                        false);
    SwitchArg lvcSpectrumSwitch("", "lvcSpectrum", "Calulate spectrum for varying cavity detuning", cmd, false);

    ValueArg<std::string> typeValue("s", "systemType", "V, Lambda, Xi, Xi2 or tls (Two Level System)", true, "lambda",
                                    "systemType", cmd);

    Params params(cmd, argc, argv); /* Parses command lines argumenrs */

    //Set system type
    if (typeValue.getValue() == "lambda" || typeValue.getValue() == "Lambda")
        type = Lambda;
    else if (typeValue.getValue() == "v" || typeValue.getValue() == "V")
        type = V;
    else if (typeValue.getValue() == "xi" || typeValue.getValue() == "Xi")
        type = Xi;
    else if (typeValue.getValue() == "xi2" || typeValue.getValue() == "Xi2")
        type = Xi2;
    else if (typeValue.getValue() == "tls")
        type = TwoLevel;
    else {
        std::cout << "Error: System type not found" << std::endl;
        return EXIT_FAILURE;
    }
    params.setSystemName(typeValue.getValue());
    params.setSystemType(type);

    //start calculation
    if (photonMapSwitch.getValue())
        photonPlot(type, params);
    if (g1Switch.getValue())
        calcG1(type, params);
    if (mapPlotSwitch.getValue())
        mapPlot(type, params);
    if (singleSwitch.getValue())
        single(type, params, false);
    if (spectrumTimeSwitch.getValue())
        spectrumTime(type, params);
    if (lvcSwitch.getValue())
        deltaLvsDeltaC(type, params);
    if (lvcSpectrumSwitch.getValue())
        lvcSpectrum(type, params);

    return 0;
}

void spectrumTime(SystemType type, Params &params) {
    SpectrumCalculator *sc = SpectrumCalculator::getPointer(params, type, params.getNormalizePart());
    double tStart = 5.0, tMax = params.getTMax(), tStep = 2;
    int n = (int) ((tMax - tStart) / tStep); //number of steps
    std::ofstream of(sc->getOfstreamName(params.getFilename()));

    sc->calcG1();
    for (int i = 0; i < n; i++) {
        params.setTMax(tStart + i * tStep);
        sc->calcSpectrum(tStart + i * tStep, of, i == 0, true);
        of << "\n"; //this is for gnuplot, but confuses mathematica
        std::cout << "t = " << tStart + i * tStep << "ps\n";
    }
    std::cout << sc->getOfstreamName(params.getFilename()) << std::endl;

    delete sc;
}

void calcG1(SystemType type, const Params &params) {
    SpectrumCalculator *sc = SpectrumCalculator::getPointer(params, type, params.getNormalizePart());
    sc->calcG1();
    std::ofstream of(sc->getOfstreamName(params.getFilename()));
    sc->calcSpectrum(params.getTMax(), of, true);

    delete sc;
}

void single(SystemType type, const Params &params, bool svOutput) {
    Timer timer;
    ThreeLevelBase *vn = initThreeLevelBase(type, params);
    SaveValues sv(svOutput, params, vn);
    auto Rho0 = vn->getRho0();

    integrateConst(type, vn, Rho0, 0.0, params.getTMax(), params.getIntegrationStep(), sv);

    timer.stop();
    sv.calcRamanPopulation();
    sv.calcAnalyticalPhotonPopulation();
    sv.writeFile(params.getFilename());
    delete vn;
}

void deltaLvsDeltaC(SystemType systemType, Params &params) {
    const double dlMax = 2.0e-3, dcMax = 2.0e-3, step = 0.1e-3;
    const unsigned int nC = (unsigned int) (2.0 * dcMax / step) + 1;
    const unsigned int nL = (unsigned int) (2.0 * dlMax / step) + 1;

    std::string base = params.getSystemName() + "_" + params.getPulseName() + "_";
    std::string nameFinal =
            base + params.getFilename() + "_" + "om" + to_string_with_precision(params.getOmega0() * 1.0e3);
    std::ofstream of(nameFinal), of_optimal(nameFinal + "_overlay");

    boost::progress_display prog(nL * nC * 5);
    double values[nL][nC];

#pragma omp parallel for collapse(2) num_threads(96)
    for (int i = 0; i < nL; i++) {
        for (int j = 0; j < nC; j++) {
            Params p = params;
            double dl = -dlMax + i * step, dc = -dcMax + j * step;
            p.setLaserDetuning(dl);
            p.setCavityDetuning(dc);
            ThreeLevelBase *tlb = initThreeLevelBase(systemType, p);
            auto Rho0 = tlb->getRho0();
            SaveValues sv(false, p, tlb);
            integrateConst(systemType, tlb, Rho0, 0.0, p.getTMax(), params.getIntegrationStep(), sv);

            values[i][j] = sv.calcEmissionProbability(sv.get_bb(), params.getIntegrationStep());
        }
    }

    //output
    of << "#" << params << std::endl;
    for (int j = 0; j < nC; j++) {
        double dc = -dcMax + j * step;
        for (int i = 0; i < nL; i++) {
            double dl = -dlMax + i * step;
            of << -dc * 1.0e3 << " " << dl * 1.0e3 << " " << values[i][j] << std::endl;

        }
        double dl_optimal = dc * pow(params.getOmega0(), 2.0)
                            / (pow(dc, 2.0) + pow(0.5 * (params.getKappa() + params.getGammaPure()), 2.0));
        of_optimal << -dc * 1.0e3 << " " << dl_optimal * 1.0e3 << " " << 0.0 << std::endl;
        of << std::endl;
    }
    std::cout << std::endl << nameFinal << std::endl;
    of.close();
};

void lvcSpectrum(SystemType systemType, Params &params) {
    const double dcMax = -1e-3, dcMin = 0.3e-3;
    const double dcStep = (dcMax - dcMin) / 150;
    const unsigned int nC = (unsigned int) ((dcMax - dcMin) / dcStep) + 1;

    std::string base = params.getSystemName() + "_" + params.getPulseName() + "_";
    std::string nameFinal =
            "spectrumLvc/" + base + params.getFilename() + "_" + "ar" + to_string_with_precision(params.getPulseArea());
    std::ofstream of(nameFinal), of_optimal(nameFinal + "_overlay");

    of << "#" << params << std::endl << std::endl;
    boost::progress_display prog(nC);
    for (int j = 0; j < nC; j++) {
        Params p = params;
        double dc = dcMin + j * dcStep;
        double dl = -dc * pow(params.getOmega0(), 2.0)
                    / (pow(dc, 2.0) + pow(0.5 * (params.getKappa() + params.getGammaPure()), 2.0));
        p.setLaserDetuning(dl);
        p.setCavityDetuning(dc);

        SpectrumCalculator *sc = SpectrumCalculator::getPointer(p, systemType, p.getNormalizePart());
        sc->calcG1(false);

        sc->calcSpectrum(p.getTMax(), of, j == 0, false, std::to_string(-dc * 1.0e3));
        of << std::endl;

        ++prog;
        delete sc;
    }

    std::cout << std::endl << nameFinal << std::endl;
};

void mapPlot(SystemType type, Params &params) {
    const double dlMax = 3.0e-3, omMax = 1.7e-3, step = 0.01e-3, omStep = 0.01e-3;
    const unsigned int nO = (unsigned int) (omMax / omStep) + 1;
    const unsigned int nL = (unsigned int) (dlMax / step) + 1;

    std::string base = "map/" + params.getSystemName() + "_" + params.getPulseName() + "_";
    std::string nameFinal = base + params.getFilename();
    std::ofstream of(nameFinal), of_optimal(nameFinal + "_overlay");

    boost::progress_display prog(nL * nO * 2);
    double values[nL][nO];//, valuesRaman[nL][nO];

#pragma omp parallel for collapse(2) num_threads(16)
    for (int i = 0; i < nL; i++) {
        for (int j = 0; j < nO; j++) {
            Params p = params;
            double dl = i * step, om = j * omStep;
            p.setLaserDetuning(dl);
            p.setOmega0(om);
            ThreeLevelBase *tlb = initThreeLevelBase(type, p);
            auto Rho0 = tlb->getRho0();
            SaveValues sv(false, p, tlb);
            integrateConst(type, tlb, Rho0, 0.0, p.getTMax(), params.getIntegrationStep(), sv);
            //negative cavity detuning according to Dominik's convention
            values[i][j] = sv.calcEmissionProbability(sv.get_bb(), params.getIntegrationStep());
            ++prog;
            //valuesRaman[i][j] = sv.calcEmissionProbability(sv.calcRamanPopulation(), integrationStep);
            ++prog;
        }
    }

    of << "#" << params << std::endl;

    for (int j = 0; j < nO; j++) {
        double om = j * omStep;
        params.setOmega0(om);
        double max = 0.0, optDet = 0.0;
        for (int i = 0; i < nL; i++) {
            //of << setprecision(3);
            double dl = i * step;
            of << params.getPulseArea() << " " << dl * 1.0e3 << " " << values[i][j] << " " << om * 1.0e3
               << std::endl;// << " " << valuesRaman[i][j] << std::endl;
            if (values[i][j] > max) {
                max = values[i][j];
                optDet = dl;
            }
        }
        double dl_optimal = params.getCavityDetuning() * pow(om, 2.0)
                            / (pow(params.getCavityDetuning(), 2.0) +
                               pow(0.5 * (params.getKappa() + params.getGammaPure()), 2.0));
        of_optimal << params.getPulseArea() << " " << max << " " << -dl_optimal * 1.0e3 << " " << optDet * 1.0e3 << " "
                   << 0.0 << " " << om * 1.0e3 << std::endl;
        of << std::endl;
    }
    std::cout << std::endl << nameFinal << std::endl;
    of.close();
}

void photonPlot(SystemType type, Params &params) {
    Timer timer;
    std::string laser = params.UsePulse() ? "-pulse" : "-cw";
    if (params.rectanglePulse()) laser = "-rect";
    std::string suffix = laser + "-om" + to_string_with_precision(params.getOmega0() * 1.0e3, 2) + "meV";
    std::ofstream of(params.getFilename() + suffix);
    const double detuningMin = -0.5, detuningMax = 2.5, step = 0.025;
    const int n = (int) ((detuningMax - detuningMin) / step);
    std::array<std::string, n> ssArr;
#pragma omp parallel for num_threads(24)
    for (int j = 0; j < n; j++) {
        double detuning = detuningMin + j * step;

        ThreeLevelBase *vn = initThreeLevelBase(type, params);
        SaveValues sv(false, params, vn);
        auto Rho0 = vn->getRho0();
        //calculation
        integrateConst(type, vn, Rho0, 0.0, params.getTMax(), params.getIntegrationStep(), sv);
        //write values to ofstream
        using namespace OpLabels;
        std::map<expectationValue, std::vector<dcompl>>
                mapValues = sv.getValues();
        std::stringstream ss;
        for (int i = 0; i < sv.getTimes().size(); i++) {
            ss << sv.getTimes()[i] << " " << detuning << " " << mapValues.at(bb)[i].real() <<
               " " << mapValues.at(s1)[i].real() << " " << mapValues.at(s2)[i].real()
               << " " << mapValues.at(s3)[i].real() << " " << mapValues.at(P32b)[i]
               << " " << mapValues.at(P31b)[i] << " " << params.laser(sv.getTimes()[i]).real() << std::endl;
        }
        ss << std::endl;
        std::cout << detuning << std::endl;
        ssArr[j] = ss.str();
        delete vn;
    }
    timer.stop();
    for (std::string &s : ssArr)
        of << s;
    of.close();
    std::cout << params.getFilename() + suffix << std::endl;
}

