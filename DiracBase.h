#ifndef DIRACBASE_H
#define DIRACBASE_H

#include <complex>
#include <ostream>

typedef std::complex<double> dcompl;

inline int delta(int i, int j) { return i == j; }

enum braKetType {
    electronic, photonic
};

enum Level {
    level1 = 1, level2, level3
};

template<braKetType t>
class DiracBase {
protected:
    int value;
    dcompl factor;
public:
    dcompl getFactor() const {
        return factor;
    }

    int getValue() const {
        return value;
    }

    DiracBase(int value, const dcompl &factor = 1.0) : value(value), factor(factor) {}

    virtual ~DiracBase() {}

    friend std::ostream &operator<<(std::ostream &os, const DiracBase<t> &temp) {
        os << "value: " << temp.value << " factor: " << temp.factor;
        return os;
    }
};

template<braKetType t>
class Bra : public DiracBase<t> {
public:
    Bra(int value, const dcompl &factor = 1.0) : DiracBase<t>(value, factor) {}

    virtual ~Bra() {}
};

template<braKetType t>
class Ket : public DiracBase<t> {
public:
    Ket(int value, const dcompl &factor = 1.0) : DiracBase<t>(value, factor) {}

    virtual ~Ket() {}
};

class KetBra {
private:
    Ket<electronic> ket;
    Bra<electronic> bra;
public:
    KetBra(const Ket<electronic> &ket, const Bra<electronic> &bra) : ket(ket), bra(bra) {}

    KetBra(Level ketLevel, Level braLevel) : ket(ketLevel), bra(braLevel) {}

    KetBra(int ketLevel, int braLevel) : ket(Level(ketLevel)), bra(Level(braLevel)) {}

    explicit KetBra(Level level) : bra(level), ket(level) {}

    explicit KetBra(int level) : bra(Level(level)), ket(Level(level)) {}

    virtual ~KetBra() {}

    const Ket<electronic> &getKet() const {
        return ket;
    }

    const Bra<electronic> &getBra() const {
        return bra;
    }

    friend std::ostream &operator<<(std::ostream &os, const KetBra &kb) {
        os << "ket: " << kb.ket << " bra: " << kb.bra;
        return os;
    }

    KetBra adj() {
        return KetBra(bra.getValue(), ket.getValue());
    }
};

class DoubleBra {
private:
    Bra<electronic> levelBra;
    Bra<photonic> photonBra;
public:
    DoubleBra(const Bra<electronic> &bra1, const Bra<photonic> &bra2) : levelBra(bra1), photonBra(bra2) {}

    DoubleBra(const DoubleBra &db) : levelBra(db.getLevelBra()), photonBra(db.getPhotonBra()) {}

    virtual ~DoubleBra() {}

    const Bra<electronic> &getLevelBra() const {
        return levelBra;
    }

    const Bra<photonic> &getPhotonBra() const {
        return photonBra;
    }

    friend std::ostream &operator<<(std::ostream &os, const DoubleBra &bra) {
        os << "stateBra: " << bra.levelBra << " photonBra: " << bra.photonBra;
        return os;
    }
};

class DoubleKet {
private:
    Ket<electronic> levelKet;
    Ket<photonic> photonKet;
public:
    DoubleKet(const Ket<electronic> &ket1, const Ket<photonic> &ket2) : levelKet(ket1), photonKet(ket2) {}

    virtual ~DoubleKet() {}

    const Ket<electronic> &getLevelKet() const { return levelKet; }

    const Ket<photonic> &getPhotonKet() const { return photonKet; }

    friend std::ostream &operator<<(std::ostream &os, const DoubleKet &ket) {
        os << "stateKet: " << ket.levelKet << " photonKet: " << ket.photonKet;
        return os;
    }
};

enum photonOperatorType {
    creation, annihilation, number
};

template<photonOperatorType t>
class PhotonOperator {
private:
    dcompl factor;
public:
    PhotonOperator(const dcompl &factor = 1.0) : factor(factor) {}

    const dcompl &getFactor() const { return factor; }
};

typedef PhotonOperator<creation> CreationOperator;
typedef PhotonOperator<annihilation> AnnihilationOperator;
typedef PhotonOperator<number> NumberOperator;

//Scaling

template<braKetType t>
inline Bra<t> operator*(const Bra<t> &bra, const dcompl& c) { return Bra<t>(bra.getValue(), bra.getFactor() * c); }

template<braKetType t>
inline Bra<t> operator*(const dcompl& c, const Bra<t> &bra) { return bra * c; }

template<braKetType t>
inline Ket<t> operator*(const Ket<t> &ket, const dcompl& c) { return Ket<t>(ket.getValue(), ket.getFactor() * c); }

template<braKetType t>
inline Ket<t> operator*(const dcompl& c, const Ket<t> &ket) { return ket * c; }

inline KetBra operator*(const dcompl& c, const KetBra &kb) { return KetBra(kb.getKet() * c, kb.getBra()); }

inline KetBra operator*(const KetBra &kb, const dcompl& c) { return c * kb; }

inline DoubleBra operator*(const DoubleBra &db, const dcompl& c) {
    return DoubleBra(db.getLevelBra() * c, db.getPhotonBra());
}

inline DoubleBra operator*(const dcompl& c, const DoubleBra &db) { return db * c; }

inline DoubleKet operator*(const DoubleKet &dk, const dcompl& c) {
    return DoubleKet(dk.getLevelKet() * c, dk.getPhotonKet());
}

inline DoubleKet operator*(const dcompl& c, const DoubleKet &dk) { return dk * c; }

//Scalar product
template<braKetType t>
inline dcompl operator*(const Bra<t> &b, const Ket<t> &k) {
    return (double) delta(b.getValue(), k.getValue()) * b.getFactor() * k.getFactor();
};

inline DoubleBra operator*(const DoubleBra &db, const KetBra &kb) {
    return DoubleBra(kb.getBra(), db.getPhotonBra()) * (db.getLevelBra() * kb.getKet());
}

inline DoubleKet operator*(const KetBra &kb, const DoubleKet &dk) {
    return DoubleKet(kb.getKet(), dk.getPhotonKet()) * (kb.getBra() * dk.getLevelKet());
}

inline dcompl operator*(const DoubleBra &db, const DoubleKet &dk) {
    return (db.getLevelBra() * dk.getLevelKet()) * (db.getPhotonBra() * dk.getPhotonKet());
}

//Photon Operator

template<photonOperatorType t>
inline PhotonOperator<t> operator*(const dcompl& c, const PhotonOperator<t> &po) {
    return PhotonOperator<t>(po.getFactor() * c);
};

template<photonOperatorType t, braKetType t2>
inline PhotonOperator<t> operator*(const PhotonOperator<t> &po, const dcompl& c) {
    return c * po;
};

template<photonOperatorType t>
inline Ket<photonic> operator*(const PhotonOperator<t> &po, const Ket<photonic> &k) {
    switch (t) {
        case creation:
            return Ket<photonic>(k.getValue() + 1, k.getFactor() * po.getFactor() * sqrt(k.getValue() + 1));
        case annihilation:
            return Ket<photonic>(k.getValue() - 1, k.getFactor() * po.getFactor() * sqrt(k.getValue()));
        case number:
            return Ket<photonic>(k.getValue(), k.getFactor() * po.getFactor() * (double) k.getValue());
    }
};


template<photonOperatorType t>
inline DoubleKet operator*(const PhotonOperator<t> &po, const DoubleKet &dk) {
    return DoubleKet(dk.getLevelKet(), po * dk.getPhotonKet());
};


#endif //DIRACBASE_H