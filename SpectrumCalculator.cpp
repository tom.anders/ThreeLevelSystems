#include "SpectrumCalculator.h"
#include "NumericalIntegration.h"

SpectrumCalculator *SpectrumCalculator::getPointer(const Params &params,  SystemType type,
                                                  const std::string &normalizePart) {
    if (type == Lambda)
        return new SpectrumCalculatorLambda(params,  type, normalizePart);
    else
        return new SpectrumCalculatorV(params,  type, normalizePart);
}


std::vector<std::vector<dcompl>>
SpectrumCalculator::calcCorrelator(const Matrix &matLeft, const std::vector<std::vector<Matrix>> &RhoSchlange) const {
    std::vector<std::vector<dcompl>> vec(RhoSchlange.size());
#pragma omp parallel for  schedule(dynamic) num_threads(96)
    for (int i = 0; i < vec.size(); i++) {
        std::vector<dcompl> tVec(RhoSchlange[i].size());
        for (int j = 0; j < RhoSchlange[i].size(); j++)
            tVec[j] = (RhoSchlange[i][j] * matLeft).trace();
        vec[i] = tVec;
    }
    return vec;
}

std::vector<std::vector<Matrix>> SpectrumCalculator::calcRhoTilde() const {
    //init
    ThreeLevelBase *vnStart = initThreeLevelBase(type,  params);
    SaveRho sr(vnStart, false);
    auto Rho0 = vnStart->getRho0();
    //first integrate to tMax
    integrateConst(type, vnStart, Rho0, 0.0, tMax, tStep, sr);
    std::vector<Matrix> rhoVec = *sr.getRho();

    std::vector<std::vector<Matrix>> RhoSchlangeVec(rhoVec.size()); //stores the calculated values
#pragma omp parallel for  schedule(dynamic) num_threads(RhoSchlangeVec.size())
    for (unsigned long i = 0; i < RhoSchlangeVec.size(); i++) {
        ThreeLevelBase *vn = initThreeLevelBase(type,  params);
        const double tStrich = i * tStep;
        const double tauMax = tMax - tStrich;
        SaveRho sr2(vn, false);
        vn->setDelay(tStrich); //important!
        MatrixDiag U = vn->getU(tStrich);
        MatrixDiag U_adj = complexConjugateDiagonal(U);

        //Now calculate Rho(t) * b and integrate to tauMax = tMax - tStrich
        Matrix lastRho = sr.getRho()->operator[](i);
        auto RhoSchlange = vn->getOpMat(AnnihilationOperator()) * (U_adj * lastRho * U);
        integrateConst(type, vn, RhoSchlange, 0.0, tauMax, tauStep, sr2);

        std::vector<Matrix> tVec(sr2.getRho()->size());
        for (unsigned long j = 0; j < tVec.size(); j++) {
            MatrixDiag U = vn->getU(j * tauStep);
            MatrixDiag U_adj = complexConjugateDiagonal(U);
            tVec[j] = (U_adj * sr2.getRho()->operator[](j) * U);
        }
        RhoSchlangeVec[i] = tVec;
        delete vn;
    }
    delete vnStart;
    return RhoSchlangeVec;
}


std::string SpectrumCalculator::nameToLabel(spectrumPart part) const {
    switch(part) {
        case cavity:
            return "\" cavity \"";
        case raman:
            return "\" raman \" ";
        case tls:
            return "\" tls \" ";
        case rest:
            return "\" rest \" ";
        case full:
            return "\" full \" ";
    }
}

SpectrumCalculator::SpectrumCalculator(const Params &params,  SystemType type, const std::string &normalizePart)
        : params(params),  type(type), tMax(params.getTMax()) {
    RhoTilde = calcRhoTilde();
    setNormalizePart(normalizePart);
    ThreeLevelBase *vnStart = initThreeLevelBase(type,  params);
    SaveValues sv(false, params, vnStart);
    auto Rho0 = vnStart->getRho0();
    integrateConst(type, vnStart, Rho0, 0.0, tMax, tStep, sv);
    photonPopulation = (std::vector<dcompl> &&) sv.getValues().at(OpLabels::bb); //The cast stops CLion from complaining
    delete vnStart;
}

void SpectrumCalculator::calcG1_cavity() {
    G1_cavity.resize(RhoTilde.size());
    for (unsigned int i = 0; i < RhoTilde.size(); i++)
        G1_cavity.at(i).resize(RhoTilde.at(i).size());

    for (unsigned int i = 0; i < G1_cavity.size(); i++) {
        for (unsigned int j = 0; j < G1_cavity.at(i).size(); j++) {
            double tau = (double) j * tauStep;
            G1_cavity.at(i).at(j) =
                    photonPopulation.at(i) * exp((im * params.getHw_c() - 0.5 * params.getKappa()) * tau / hBar);
        }
    }
}

void SpectrumCalculator::calcG1(bool verbose) {
    if (verbose) std::cout << "cavity" << std::endl;
    calcG1_cavity();
    if (verbose) std::cout << "raman" << std::endl;
    calcG1_Raman();
    if (verbose) std::cout << "tls" << std::endl;
    calcG1_tls();
    if (verbose) std::cout << "rest" << std::endl;
    calcG1_rest();

    G1 = {{cavity, G1_cavity},
          {raman,  G1_raman},
          {tls,    G1_tls},
          {rest,   G1_rest},
          {full,   calcCorrelator(ThreeLevelBase::getOpMat(CreationOperator()), RhoTilde)}};

}


void SpectrumCalculator::calcSpectrum(double t, std::ofstream &of, bool writeHeader, bool writeTime, std::string prefix) {
    const int steps = 100;
    std::vector<double> vE(steps);
    std::map<spectrumPart, std::vector<double>> vS;
    for (const spectrumPart &s : gNames)
        vS[s].resize(steps);

    const double E_min = params.getHw_c() - 0.8e-3, E_max = params.getHw_c() + 1.2e-3;
#pragma omp parallel for num_threads(192)
    for (unsigned int k = 0; k < steps; k++) {
        double E = E_min + k * (E_max - E_min) / steps;
        vE[k] = (E - params.getHw_c()) * 1.0e3;
        for (const spectrumPart &s : gNames) {
            vS[s][k] = spectrumAt(E, t, s);
        }
    }
    writeSpectrum(vE, vS, t, of, writeHeader, writeTime, steps, prefix);
}


double SpectrumCalculator::spectrumAt(const double E, const double t, const spectrumPart s) {
    unsigned int tInt = (unsigned int) (t / tStep) + 1;
        std::vector<dcompl> outerIntegral(tInt);
#pragma omp parallel for num_threads(96) schedule(dynamic)
        for (int i = 0; i < tInt; i++) {
            std::vector<dcompl> innerIntegral(G1[s][i].size());
            for (int j = 0; j < innerIntegral.size(); j++)
                if ((double) j * tauStep <= t - (double) i * tStep)
                    innerIntegral[j] = G1[s][i][j] * exp(-im * E / hBar * (double) j * tauStep);
                else
                    innerIntegral[j] = 0.0;

            outerIntegral[i] = simpson(innerIntegral,  tauStep);
        }
        return simpson(outerIntegral, tStep).real();
}

void SpectrumCalculator::writeSpectrum(std::vector<double> vE, std::map<spectrumPart, std::vector<double>> vS, double t,
                                       std::ofstream &of, bool writeHeader,
                                       bool writeTime, unsigned long steps, std::string prefix) const {

    std::vector<double> vSum(steps);
    for (int i = 0; i < vE.size(); i++) {
        double sum = 0.0;
        for (const spectrumPart &s : gNames) {
            if (s != full)
                sum += vS[s][i];
        }
        vSum[i] = sum;
    }

    double max = normalize ? *std::max_element(vS.at(normalizePart).begin(), vS.at(normalizePart).end()) : 1.0;
    if(normalize) {
        std::cout << "normalizePart: " << normalizePart << std::endl;
        std::cout << "max: " << max << std::endl;
    }

    //columnheaders
    if (writeHeader) {
        of << "xyz"; //just so gnuplot doesn't mess up
        for (const spectrumPart &s : gNames) {
            of << nameToLabel(s) << " ";
        }
        of << " \" sum \" " << std::endl;
        of << params << std::endl;
    }

    //output
    for (int i = 0; i < vE.size(); i++) {
        if (writeTime)
            of << t << " ";
        of << prefix << " ";
        of << vE[i] << " ";
        for (const spectrumPart &s : gNames) {
            of << vS[s][i] / max << " ";
        }
        of << vSum[i] << " ";
        of << "\n";
    }

}

std::string SpectrumCalculator::getOfstreamName(const std::string &name) {
    std::string base = "spectrum/" + params.getSystemName() + "/" + params.getPulseName() + "/";
    std::string nameFinal = base + name + "_" + "ar" + to_string_with_precision(params.getPulseArea())
                       + "dc" + to_string_with_precision(abs(params.getCavityDetuning() * 1.0e3)) + ".txt";
    std::cout << nameFinal << std::endl;
    return nameFinal;
}


/*
 * Lambda System
 */

SpectrumCalculatorLambda::SpectrumCalculatorLambda(const Params &params,  SystemType type, const std::string &normalizePart)
        :
        SpectrumCalculator(params,  type, normalizePart ){
    P13 = calcCorrelator(ThreeLevelBase::getOpMat(KetBra(level1, level3)), RhoTilde);
    P23Corr = calcCorrelator(ThreeLevelBase::getOpMat(KetBra(level2, level3)), RhoTilde);
    Sigma23b = calcCorrelator(
            (ThreeLevelBase::getOpMat(KetBra(level2)) - ThreeLevelBase::getOpMat(KetBra(level3)))
            * ThreeLevelBase::getOpMat(CreationOperator()), RhoTilde);
}

void SpectrumCalculatorLambda::calcG1_tls() {
    G1_tls.resize(RhoTilde.size());
    for (unsigned int i = 0; i < RhoTilde.size(); i++) {
        G1_tls.at(i).resize(RhoTilde.at(i).size()); //may not be needed
    }

    for (int i = 0; i < G1_tls.size(); i++) {
        std::vector<dcompl> intVec = simpsonToT([this](int j) {
            double tau = (double) j * tauStep;
            return exp((-im * params.getCavityDetuning()
                        + 0.5 * (params.getKappa() - params.getGammaPure())) * tau / hBar);
        }, RhoTilde.at(i).size(), tauStep);
        G1_tls.at(i) = multiplVector(intVec, [this, i](double tau) {
            return im * params.getG() / hBar * P23Corr.at(i).at(0) *
                   exp((im * params.getHw_c() - 0.5 * params.getKappa()) * tau / hBar);
        }, tauStep);
    }
}

void SpectrumCalculatorLambda::calcG1_Raman() {
    ThreeLevelBase *vn = initThreeLevelBase(type,  params);
    double hw_l = vn->getHwl();
    G1_raman.resize(RhoTilde.size());
    for (unsigned int i = 0; i < RhoTilde.size(); i++)
        G1_raman.at(i).resize(RhoTilde.at(i).size()); //may not be needed

    for (int i = 0; i < G1_raman.size(); i++) {
        std::vector<dcompl> innerInt = simpsonToT([this, i, vn, hw_l](int j) {
            double t = (double) i * tStep;
            double tau = (double) j * tauStep;
            return exp((0.5 * params.getGammaPure() - im * (vn->getE_2() - vn->getE_3())) * tau / hBar)
                   * params.laser(t + tau) * exp(im * hw_l * (t + tau) / hBar) * P13.at(i).at(j);
        }, RhoTilde.at(i).size(), tauStep);
        std::vector<dcompl> outerInt = simpsonToT([this, innerInt](int j) {
            double tau = j * tauStep;
            return exp((-im * params.getCavityDetuning() +
                        0.5 * (params.getKappa() - params.getGammaPure())) * tau / hBar)
                   * innerInt.at(j);
        }, RhoTilde.at(i).size(), tauStep);
        G1_raman.at(i) = multiplVector(outerInt, [this](double tau) {
            return -params.getG() / pow(hBar, 2) *
                   exp((im * params.getHw_c() - 0.5 * params.getKappa()) * tau / hBar);
        }, tauStep);
    }
}

void SpectrumCalculatorLambda::calcG1_rest() {
    ThreeLevelBase *vn = initThreeLevelBase(type,  params);
    double hw_l = vn->getHwl();
    G1_rest.resize(RhoTilde.size());
    for (unsigned int i = 0; i < RhoTilde.size(); i++)
        G1_rest.at(i).resize(RhoTilde.at(i).size()); //may not be needed

    for (unsigned long i = 0; i < G1_tls.size(); i++) {
        std::vector<dcompl> innerInt = simpsonToT([this, i, vn, hw_l](int j) {
            double t = (double) i * tStep;
            double tau = (double) j * tauStep;
            return exp((0.5 * params.getGammaPure() - im * (vn->getE_2() - vn->getE_3())) * tau / hBar)
                   * Sigma23b.at(i).at(j);
        }, RhoTilde.at(i).size(), tauStep);
        std::vector<dcompl> outerInt = simpsonToT([this, innerInt](int j) {
            double tau = j * tauStep;
            return exp((-im * params.getCavityDetuning() +
                        0.5 * (params.getKappa() - params.getGammaPure())) * tau / hBar)
                   * innerInt.at(j);
        }, RhoTilde.at(i).size(), tauStep);
        G1_rest.at(i) = multiplVector(outerInt, [this](double tau) {
            return pow(params.getG() / hBar, 2) *
                   exp((im * params.getHw_c() - 0.5 * params.getKappa()) * tau / hBar);
        }, tauStep);
    }
}

/*
 * V System
 */

SpectrumCalculatorV::SpectrumCalculatorV(const Params &params,  SystemType type, const std::string &normalizePart)
        :
        SpectrumCalculator(params,  type, normalizePart) {
    P13 = calcCorrelator(ThreeLevelBase::getOpMat(KetBra(level1, level3)), RhoTilde);
    P12Corr = calcCorrelator(ThreeLevelBase::getOpMat(KetBra(level1, level2)), RhoTilde);
    Sigma12b = calcCorrelator(
            (ThreeLevelBase::getOpMat(KetBra(level1)) - ThreeLevelBase::getOpMat(KetBra(level2)))
            * ThreeLevelBase::getOpMat(CreationOperator()), RhoTilde);
}


void SpectrumCalculatorV::calcG1_tls() {
    G1_tls.resize(RhoTilde.size());
    for (unsigned int i = 0; i < RhoTilde.size(); i++) {
        G1_tls.at(i).resize(RhoTilde.at(i).size()); //may not be needed
    }

    for (int i = 0; i < G1_tls.size(); i++) {
        std::vector<dcompl> intVec = simpsonToT([this](int j) {
            double tau = (double) j * tauStep;
            return exp((-im * params.getCavityDetuning()
                        + 0.5 * (params.getKappa() - params.getGammaPure())) * tau / hBar);
        }, RhoTilde.at(i).size(), tauStep);
        G1_tls.at(i) = multiplVector(intVec, [this, i](double tau) {
            return im * params.getG() / hBar * P12Corr.at(i).at(0) *
                   exp((im * params.getHw_c() - 0.5 * params.getKappa()) * tau / hBar);
        }, tauStep);
    }
}

void SpectrumCalculatorV::calcG1_Raman() {
    ThreeLevelBase *vn = initThreeLevelBase(type,  params);
    double hw_l = vn->getHwl();
    G1_raman.resize(RhoTilde.size());
    for (unsigned int i = 0; i < RhoTilde.size(); i++)
        G1_raman.at(i).resize(RhoTilde.at(i).size()); //may not be needed

    for (int i = 0; i < G1_raman.size(); i++) {
        std::vector<dcompl> innerInt = simpsonToT([this, i, vn, hw_l](int j) {
            double t = (double) i * tStep;
            double tau = (double) j * tauStep;
            return exp((0.5 * params.getGammaPure() - im * (vn->getE_1() - vn->getE_2())) * tau / hBar)
                   * params.laser(t + tau) * exp(im * hw_l * (t + tau) / hBar) * P13.at(i).at(j);
        }, RhoTilde.at(i).size(), tauStep);
        std::vector<dcompl> outerInt = simpsonToT([this, innerInt](int j) {
            double tau = j * tauStep;
            return exp((-im * params.getCavityDetuning() +
                        0.5 * (params.getKappa() - params.getGammaPure())) * tau / hBar)
                   * innerInt.at(j);
        }, RhoTilde.at(i).size(), tauStep);
        G1_raman.at(i) = multiplVector(outerInt, [this](double tau) {
            return params.getG() / pow(hBar, 2) *
                   exp((im * params.getHw_c() - 0.5 * params.getKappa()) * tau / hBar);
        }, tauStep);
    }
}

void SpectrumCalculatorV::calcG1_rest() {
    ThreeLevelBase *vn = initThreeLevelBase(type,  params);
    G1_rest.resize(RhoTilde.size());
    for (unsigned int i = 0; i < RhoTilde.size(); i++)
        G1_rest.at(i).resize(RhoTilde.at(i).size()); //may not be needed

    for (unsigned long i = 0; i < G1_rest.size(); i++) {
        std::vector<dcompl> innerInt = simpsonToT([this, i, vn](int j) {
            double t = (double) i * tStep;
            double tau = (double) j * tauStep;
            return exp((0.5 * params.getGammaPure() - im * (vn->getE_1() - vn->getE_2())) * tau / hBar)
                    * Sigma12b.at(i).at(j);
        }, RhoTilde.at(i).size(), tauStep);
        std::vector<dcompl> outerInt = simpsonToT([this, innerInt](int j) {
            double tau = j * tauStep;
            return exp((-im * params.getCavityDetuning() +
                        0.5 * (params.getKappa() - params.getGammaPure())) * tau / hBar)
                   * innerInt.at(j);
        }, RhoTilde.at(i).size(), tauStep);
        G1_rest.at(i) = multiplVector(outerInt, [this](double tau) {
            return pow(params.getG() / hBar, 2) *
                   exp((im * params.getHw_c() - 0.5 * params.getKappa()) * tau / hBar);
        }, tauStep);
    }
}

void SpectrumCalculator::setNormalizePart(const std::string &part) {
    if (part == "raman")
        normalizePart = raman;
    else if (part == "tls")
        normalizePart = tls;
    else if (part == "cavity")
        normalizePart = cavity;
    else if (part == "rest")
        normalizePart = rest;
    else if (part == "full")
        normalizePart = full;
    else normalize = false;
}



