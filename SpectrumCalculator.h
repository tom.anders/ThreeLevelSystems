#ifndef SPECTRUMCALCULATOR_H
#define SPECTRUMCALCULATOR_H

#include <map>
#include "ThreeLevelBase.h"
#include "RungeKuttaIntegration.h"
#include <boost/progress.hpp>
#include <boost/timer.hpp>

typedef std::vector<std::vector<dcompl>> dcomplVec;

enum spectrumPart {
    cavity, raman, tls, rest, full
};

class SpectrumCalculator {
protected:
    Params params;
    SystemType type;

    std::vector<spectrumPart> gNames = {cavity, raman, tls, rest, full};

    spectrumPart normalizePart;
    bool normalize = true;

    dcomplVec G1_cavity;
    dcomplVec G1_tls;
    dcomplVec G1_raman;
    dcomplVec G1_rest;

    std::map<spectrumPart, dcomplVec> G1; //Components of the spectrum

    std::vector<dcompl> photonPopulation;

    std::vector<std::vector<Matrix>> RhoTilde; //Stores the calculates values of b*Rho(t)

    //integration steps
    double tStep = 0.1;
    double tauStep = 0.1;

    const double tMax;

    //Calculates correlator <matLeft(t+tau)b(t)>
    std::vector<std::vector<dcompl>> calcCorrelator(const Matrix &matLeft, const std::vector<std::vector<Matrix>> &RhoTilde) const;

    std::vector<std::vector<Matrix>> calcRhoTilde() const;

    //Converts the names for the different parts to labels for GnuPlot
    std::string nameToLabel(spectrumPart part) const;

    void setNormalizePart(const std::string &part);

    void calcG1_cavity();

    virtual void calcG1_tls() = 0;

    virtual void calcG1_Raman() = 0;

    virtual void calcG1_rest() = 0;

public:
    SpectrumCalculator(const Params &params,  SystemType type, const std::string &normalizePart);

    void calcG1(bool verbose = true);


    //returns pointer to lambda or v class
    static SpectrumCalculator *getPointer(const Params &params,  SystemType type,
                                          const std::string &normalizePart);

    //calculate the spectrum from G1
    void calcSpectrum(double t, std::ofstream &of, bool writeHeader = true, bool writeTime = false,
                      std::string prefix = "");

    double spectrumAt(const double E, const double t, const spectrumPart s);

    //writes spectrum to file
    void writeSpectrum(std::vector<double> vE, std::map<spectrumPart, std::vector<double>> vS, double t, std::ofstream &of,
                       bool writeHeader,
                       bool writeTime, unsigned long steps, std::string prefix = "") const;

    std::string getOfstreamName(const std::string &name);

    virtual ~SpectrumCalculator() {};
};

class SpectrumCalculatorLambda : public SpectrumCalculator {
private:
    dcomplVec P13, P23Corr, Sigma23b;
protected:
    virtual void calcG1_tls();

    virtual void calcG1_Raman();

    virtual void calcG1_rest();

public:
    SpectrumCalculatorLambda(const Params &params,  SystemType type, const std::string &normalizePart);;
};


class SpectrumCalculatorV : public SpectrumCalculator {
private:
    dcomplVec P13, Sigma12b, P12Corr;
protected:
    virtual void calcG1_tls();

    virtual void calcG1_Raman();

    virtual void calcG1_rest();

public:
    SpectrumCalculatorV(const Params &params,  SystemType type, const std::string &normalizePart);;
};

#endif //SPECTRUMCALCULATOR_H
