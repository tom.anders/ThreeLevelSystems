#include <cmath>
#include "SaveValues.h"
#include "RungeKuttaIntegration.h"
#include "NumericalIntegration.h"

SaveValues::SaveValues(bool output, const Params &params, ThreeLevelBase *vn) : output(output), params(params), vn(vn),
                                                                                mapValues(
                                                                                        new std::map<OpLabels::expectationValue, std::vector<dcompl>>),
                                                                                mapOperators(
                                                                                        new std::map<OpLabels::expectationValue, Matrix>),
                                                                                times(new std::vector<double>) {
    (*mapOperators)[OpLabels::bb] = ThreeLevelBase::getOpMat(NumberOperator());
    (*mapOperators)[OpLabels::s1] = ThreeLevelBase::getOpMat(KetBra(level1));
    (*mapOperators)[OpLabels::s2] = ThreeLevelBase::getOpMat(KetBra(level2));
    (*mapOperators)[OpLabels::s3] = ThreeLevelBase::getOpMat(KetBra(level3));
    (*mapOperators)[OpLabels::sigma32] = ThreeLevelBase::getOpMat(KetBra(level3))
                                         - ThreeLevelBase::getOpMat(KetBra(level2));
    (*mapOperators)[OpLabels::sigma21] = ThreeLevelBase::getOpMat(KetBra(level2))
                                         - ThreeLevelBase::getOpMat(KetBra(level1));
    (*mapOperators)[OpLabels::P32b] = ThreeLevelBase::getOpMat(KetBra(level3, level2), CreationOperator());
    (*mapOperators)[OpLabels::P31b] = ThreeLevelBase::getOpMat(KetBra(level3, level1), CreationOperator());
    (*mapOperators)[OpLabels::P21b] = ThreeLevelBase::getOpMat(KetBra(level2, level1), CreationOperator());
    (*mapOperators)[OpLabels::sigma32bb] =
            ThreeLevelBase::getOpMat(KetBra(level3), NumberOperator())
            - ThreeLevelBase::getOpMat(KetBra(level2), NumberOperator());
    (*mapOperators)[OpLabels::sigma21bb] = ThreeLevelBase::getOpMat(KetBra(level2), NumberOperator())
                                           - ThreeLevelBase::getOpMat(KetBra(level1), NumberOperator());
    (*mapOperators)[OpLabels::b] = ThreeLevelBase::getOpMat(AnnihilationOperator());
    (*mapOperators)[OpLabels::bk] = ThreeLevelBase::getOpMat(CreationOperator());
}

//called by odeint at each integration step
void SaveValues::operator()(const Matrix &Rho, const double &t) {
    MatrixDiag U = vn->getU(t), U_adj = complexConjugateDiagonal(U);
    Matrix RhoS = U_adj * (Rho * U); //Rho is in interaction picture, so transform it to Schrödinger picture
    for (const OpLabels::expectationValue &e : names) {
        dcompl tr = 0.0;
        for (int i = 0; i < maxStates; ++i)
            for (int k = 0; k < maxStates; ++k)
                tr += RhoS(i, k) * (*mapOperators)[e](k, i);
        (*mapValues)[e].push_back(tr); //save expectation value
    }
    times->push_back(t); //save time value
    if (output) std::cout << t << std::endl;
}

void SaveValues::writeFile(const std::string &name) const {
    std::ofstream of(name + ".txt");
    of << "#" << params << std::endl;
    std::vector<double> ramanPopulation = calcRamanPopulation();
    std::vector<double> cavityPopulation = calcCavityPhotonPopulation();
    std::vector<double> electronicPopulation = calcElectronicPopulation();
    for (unsigned int i = 0; i < times->size(); i++) {
        of << times->at(i) * (params.useTimeGt() ? params.getG() / hBar : 1.0) << " ";
        of << get_bb().at(i).real() << " ";
        of << tlsAnalyticalPhotons(times->at(i)) << " ";
        of << ramanPopulation.at(i) << " ";
        of << cavityPopulation.at(i) << " ";
        of << electronicPopulation.at(i) << " ";
        of << get_s1().at(i).real() << " ";
        of << get_s2().at(i).real() << " ";
        of << get_s3().at(i).real() << " ";
        of << tlsAnalyticalState1(times->at(i)) << " ";
        of << tlsAnalyticalState2(times->at(i)) << " ";
        of << tlsAnalyticalInversion(times->at(i)) << " ";
        of << abs(get_b().at(i)) << " ";
        of << abs(get_bk().at(i)) << " ";
        of << params.laser(times->at(i)).real() << " ";
        of << std::endl;
    }
    of.close();
    std::cout << std::endl << name + ".txt" << std::endl;
}

std::vector<double> SaveValues::analyticalPhotonIntegral(const std::vector<dcompl> &PiVec) const {
    double step = getStep();
    std::vector<dcompl> inner = simpsonToT(multiplVector(PiVec, [this, step](int j) {
        double tStrich = ((double) j) * step;
        return exp(params.getKappa() / hBar * tStrich);
    }), step);
    std::vector<double> res(inner.size());
#pragma omp parallel for num_threads(12)
    for (unsigned long i = 0; i < res.size(); i++) {
        double t = ((double) i) * step;
        res[i] = exp(-params.getKappa() / hBar * t) * 2.0 * params.getG() / hBar * inner[i].imag();
    }
    return res;
}

std::vector<double> SaveValues::calcAnalyticalPhotonPopulation() const {
    return analyticalPhotonIntegral(params.getSystemType() == Lambda ? get_P32b() : get_P21b());
}

std::vector<double> SaveValues::calcRamanPopulation() const {
    double sign = params.getSystemType() == Lambda ? -1.0 : 1.0;
    std::vector<dcompl> ramanVec = get_P31b();
    double step = getStep();

    std::vector<dcompl> inner = simpsonToT(multiplVector(ramanVec, [this, step, sign](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return sign * exp(im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich)
               * im * params.laser(tStrich) / hBar * exp(-im * vn->getHwl() / hBar * tStrich);
    }), step);

    return analyticalPhotonIntegral(multiplVector(inner, [this, step](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return exp(-im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich);
    }));
}

std::vector<double> SaveValues::calcElectronicPopulation() const {
    std::vector<dcompl> nVec = params.getSystemType() == Lambda ? get_s2() : get_s1();
    double step = getStep();

    std::vector<dcompl> inner = simpsonToT(multiplVector(nVec, [this, step](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return exp(im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich)
               * im * params.getG() / hBar;
    }), step);

    return analyticalPhotonIntegral(multiplVector(inner, [this, step](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return exp(-im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich);
    }));
}

std::vector<double> SaveValues::calcCavityPhotonPopulation() const {
    std::vector<dcompl> sigmaVec = params.getSystemType() == Lambda ? get_sigma32() : get_sigma21();
    std::vector<dcompl> photonVec = get_bb();

    double step = getStep();

    std::vector<dcompl> inner = simpsonToT(multiplVector(sigmaVec, photonVec, [this, step](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return exp(im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich)
               * (-im * params.getG() / hBar);
    }), step);

    return analyticalPhotonIntegral(multiplVector(inner, [this, step](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return exp(-im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich);
    }));
}

std::vector<double> SaveValues::calcTripletPhotonPopulation() const {
    std::vector<dcompl> sigmaVecbb = params.getSystemType() == Lambda ? get_sigma32bb() : get_sigma21bb();
    std::vector<dcompl> sigmaVec = params.getSystemType() == Lambda ? get_sigma32() : get_sigma21();
    std::vector<dcompl> sigmaTimesbbVec = multiplVector(sigmaVec, get_bb(), [](int dummy) { return 1.0; });

    double step = getStep();

    std::vector<dcompl> inner = simpsonToT(multiplVector(subtVector(sigmaVecbb, sigmaTimesbbVec), [this, step](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return exp(im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich)
               * (-im * params.getG() / hBar);
    }), step);

    return analyticalPhotonIntegral(multiplVector(inner, [this, step](int j) {
        double kappa = params.getKappa(), gamma = params.getGammaPure(), dCav = -params.getCavityDetuning();
        double tStrich = ((double) j) * step;
        return exp(-im * (dCav / hBar - im * 0.5 * (kappa + gamma) / hBar) * tStrich);
    }));
}

const double SaveValues::calcEmissionProbability(const std::vector<dcompl> &photonVec, const double step) const {
    return params.getKappa() / hBar * simpson(photonVec, step).real();
}

const double SaveValues::calcEmissionProbability(const std::vector<double> &photonVec, const double step) const {
    return params.getKappa() / hBar * simpson(photonVec, step);
}

const double SaveValues::tlsAnalyticalInversion(const double t) const {
    return cos(2.0 * params.getG() * sqrt(params.getInitialPhotons() + 1) / hBar * t);
}

const double SaveValues::tlsAnalyticalState2(const double t) const {
    return 1.0 - (4.0 * pow(params.getG(), 2) / (pow(params.getCavityDetuning(), 2) + 4.0 * pow(params.getG(), 2)))
                 *
                 pow(sin(sqrt(pow(params.getCavityDetuning(), 2) + 4.0 * pow(params.getG(), 2)) * t / (2.0 * hBar)), 2);
}

const double SaveValues::tlsAnalyticalState1(const double t) const {
    return (4.0 * pow(params.getG(), 2) / (pow(params.getCavityDetuning(), 2) + 4.0 * pow(params.getG(), 2)))
           * pow(sin(sqrt(pow(params.getCavityDetuning(), 2) + 4.0 * pow(params.getG(), 2)) * t / (2.0 * hBar)), 2);
}

const double SaveValues::tlsAnalyticalPhotons(const double t) const {
    return params.getInitialPhotons() + pow(sin(sqrt(params.getInitialPhotons() + 1) * params.getG() * t / hBar), 2);
}
