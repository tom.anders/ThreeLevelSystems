#include "ThreeLevelBase.h"

/*
 * ThreeLevelBase
 */

const Matrix ThreeLevelBase::getInteractionHamiltonian(const double t, const MatrixDiag &U, const MatrixDiag &U_adj) const {
    Matrix H_i;
    double hw_l = abs(E_1 - E_3 - params.getHw_c()) + params.getLaserDetuning();
    for (int i = 0; i < maxStates; ++i) {
        for (int j = i; j < maxStates; ++j) { // H_i is always hermitian, so we can save some time here.
            DoubleBra bra = ProductState<maxPhotons>(i).getBra();
            DoubleKet ket = ProductState<maxPhotons>(j).getKet();
            H_i(i, j) = getInteractionHamiltonianEntry(bra, ket, t, hw_l);
            H_i(j, i) = conj(H_i(i, j));
        }
    }
    return U * H_i * U_adj;
}

ThreeLevelBase::ThreeLevelBase( const Params &params, const double E_1, const double E_2, const double E_3)
        : params(params), E_1(E_1), E_2(E_2), E_3(E_3) {
    for (int i = 0; i < maxStates; ++i) { //H_0 is diagonal, so we only need one loop
        DoubleBra bra = ProductState<maxPhotons>(i).getBra();
        DoubleKet ket = ProductState<maxPhotons>(i).getKet();
        H_0(i, i) = bra * E_3 * KetBra(level3) * ket
                    + bra * E_1 * KetBra(level1) * ket
                    + bra * E_2 * KetBra(level2) * ket
                    + bra * params.getHw_c() * (NumberOperator() * ket);
    }
}

void ThreeLevelBase::operator()(const Matrix &Rho, Matrix &dRhodt, const double t) {
    MatrixDiag U = getU(t); //time evolution operator
    MatrixDiag U_adj = complexConjugateDiagonal(U);
    Matrix RhoS = U_adj * (Rho * U); //calculate Rho in Schrödinger picture
    Matrix H = getInteractionHamiltonian(t + delay, U, U_adj);

    // dissipative terms
    Matrix L = U * ((params.getKappa() * LCavity(RhoS) + params.getGammaRad() * LRad(RhoS)
                     - params.getGammaPure() * LPure(RhoS)) * (0.5 / hBar)) * U_adj;
    //Von Neumann equation
    dRhodt = -im / hBar * (H * Rho - Rho * H) + L;
}

const Matrix ThreeLevelBase::getOpMatDiag(const KetBra &kb) {
    Matrix Mat(0.0);
    for (int i = 0; i < maxStates; ++i) {
        ProductState<maxPhotons> productState(i);
        Mat(i, i) = productState.getBra() * kb * productState.getKet();
    }
    return Mat;
}

const Matrix ThreeLevelBase::getOpMat(const KetBra &kb) {
    Matrix Mat(0.0);
    for (int i = 0; i < maxStates; ++i) {
        for (int j = 0; j < maxStates; ++j) {
            ProductState<maxPhotons> p1(i), p2(j);
            Mat(i, j) = p1.getBra() * kb * p2.getKet();
        }
    }
    return Mat;
}

const Matrix ThreeLevelBase::LCavity(const Matrix &Rho) const {
    if (!params.UseKappa()) return Matrix(0.0);
    return 2.0 * getOpMat(AnnihilationOperator()) * (Rho * getOpMat(CreationOperator()))
           - Rho * getOpMatDiagType(NumberOperator())
           - getOpMatDiagType(NumberOperator()) * Rho;
}

const Matrix ThreeLevelBase::LRad(const Matrix &Rho) const {
    if (!params.UseGammaRad()) return Matrix(0.0);
    KetBra kb21(level2, level1);
    return 2.0 * getOpMat(kb21) * Rho * getOpMat(kb21.adj())
           - Rho * getOpMatDiag(KetBra(level1))
           - getOpMatDiag(KetBra(level1)) * Rho;
}

const Matrix ThreeLevelBase::LPure(const Matrix &Rho) const {
    if (!params.UseGammaPure()) return Matrix(0.0);
    Matrix L = Rho;
    for (int i = 0; i < maxStates; i++)
        for (int j = 0; j < maxStates; j++)
            if (ProductState<maxPhotons>(i).getLevel() == ProductState<maxPhotons>(j).getLevel())
                L(i, j) = 0.0;
    return L;
}

const Matrix ThreeLevelBase::getRho0() const {
    Matrix Rho0(0.0);
    Rho0(0 * (maxPhotons + 1) + params.getInitialPhotons(), 0 * (maxPhotons + 1) + params.getInitialPhotons()) = 1.0; // initial condition: state = state1, photons = 0;
    return Rho0;
}

const MatrixDiag ThreeLevelBase::getOpMatDiagType(const NumberOperator &p) {
    MatrixDiag Mat;
    for (int i = 0; i < maxStates; ++i) {
        ProductState<maxPhotons> productState(i);
        Mat(i, i) = productState.getBra() * (p * productState.getKet());
    }
    return Mat;
}

/*
 * LambdaSystem
 */

const dcompl LambdaSystem::getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const {
    return bra * KetBra(level1, level2) * params.laser(t) * exp(im * hw_l / hBar * t) * ket
           + bra * KetBra(level2, level1) * params.laser(t) * exp(-im * hw_l / hBar * t) * ket
           + bra * params.getG() * KetBra(level3, level2) * (CreationOperator() * ket)
           + bra * params.getG() * KetBra(level2, level3) * (AnnihilationOperator() * ket);
}

LambdaSystem::LambdaSystem(const Params &params) : ThreeLevelBase( params, E_1, E_2, E_3) {}

/*
 * Xi System
 */

const dcompl XiSystem::getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const {
    return bra * KetBra(level1, level2) * params.laser(t) * exp(-im * hw_l / hBar * t) * ket
           + bra * KetBra(level2, level1) * params.laser(t) * exp(im * hw_l / hBar * t) * ket
           + bra * params.getG() * KetBra(level3, level2) * (CreationOperator() * ket)
           + bra * params.getG() * KetBra(level2, level3) * (AnnihilationOperator() * ket);
}

XiSystem::XiSystem( const Params &params) : ThreeLevelBase( params, E_1, E_2, E_3) {}

/*
 * VSystem
 */

const dcompl VSystem::getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const {
    return bra * KetBra(level2, level3) * params.laser(t) * exp(im * hw_l / hBar * t) * ket
           + bra * KetBra(level3, level2) * params.laser(t) * exp(-im * hw_l / hBar * t) * ket
           + bra * params.getG() * KetBra(level2, level1) * (CreationOperator() * ket)
           + bra * params.getG() * KetBra(level1, level2) * (AnnihilationOperator() * ket);
}

VSystem::VSystem( const Params &params) : ThreeLevelBase( params, E_1, E_2, E_3) {}

/*
 * Xi2System
 */

const dcompl Xi2System::getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const {
    return bra * KetBra(level2, level3) * params.laser(t) * exp(-im * hw_l / hBar * t) * ket
           + bra * KetBra(level3, level2) * params.laser(t) * exp(im * hw_l / hBar * t) * ket
           + bra * params.getG() * KetBra(level2, level1) * (CreationOperator() * ket)
           + bra * params.getG() * KetBra(level1, level2) * (AnnihilationOperator() * ket);
}

Xi2System::Xi2System( const Params &params) : ThreeLevelBase( params, E_1, E_2, E_3) {}

/*
 * TwoLevelSystem
 */

const dcompl TwoLevelSystem::getInteractionHamiltonianEntry(const DoubleBra &bra, const DoubleKet &ket, double t, double hw_l) const {
    return bra * params.getG() * KetBra(level2, level1) * (CreationOperator() * ket)
           + (params.useNoRWA() ?
              bra * params.getG() * KetBra(level1, level2) * (CreationOperator() * ket)
              + bra * params.getG() * KetBra(level2, level1) * (AnnihilationOperator() * ket) : 0.0)
           + bra * params.getG() * KetBra(level1, level2) * (AnnihilationOperator() * ket);
}

TwoLevelSystem::TwoLevelSystem( const Params &params) : ThreeLevelBase( params, E_1, E_2, E_3) {}


