#ifndef MAIN_H
#define MAIN_H

#include "ThreeLevelBase.h"

//calculates photon population and inversion for a range of detunings
void photonPlot(SystemType type, Params &params);

//calculate photon population & inversion with variation of a certain parameter
void mapPlot(SystemType type, Params &params);

//single calculation for default parameters and a certain detuning
void single(SystemType type, const Params &params, bool svOutput);;

void calcG1(SystemType type, const Params &params);

void spectrumTime(SystemType type, Params &params);

void deltaLvsDeltaC(SystemType systemType, Params &params);

void lvcSpectrum(SystemType systemType, Params &params);

int main(int argc, char **argv);

#endif //MAIN_H
