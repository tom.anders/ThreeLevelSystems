# Benötigte Bibliotheken
 - [Boost](http://www.boost.org/users/download/)
 - [TCLAP](http://tclap.sourceforge.net/)
 - [OpenMP](http://www.openmp.org/resources/openmp-compilers/)
